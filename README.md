
# PID Synthesizer JSFX

JesuSonic FX (JSFX) implementations of the [PID Synthesis (PIDS)](https://arxiv.org/abs/2109.10455) specification. These JSFX scripts can be used for music production in Cockos REAPER & other DAWs.

This repository consists of:

1. Library that implements core PIDS functionalities. It can be integrated into any other JSFX script.
2. Demo plugins that demonstrate:
   - A 2x-oscillator PIDS synthesizer utilizing the PIDS library.
   - Integrating the [PIDEG JSFX library](https://gitlab.com/pid-envelope/pideg-jsfx) with PIDS to apply envelopes on the synthesized sounds.

## Installation

### Pre-requisite:

For the library to work, [`st-oversampler.jsfx-inc`](https://stash.reaper.fm/v/26656/wuff.zip) must be installed in the REAPER/Effects directory.

**Linux:**

1. Clone this repo to local

```sh
git clone https://gitlab.com/pidosc/pids-jsfx
```

2. Create a soft simlink of the scripts to Reaper Effects directory

```sh
ln -s "<local_path_to_this_repo>/PIDS_lib.jsfx-inc" "~/.config/REAPER/Effects/"
ln -s "<local_path_to_this_repo>/PIDEG_lib.jsfx-inc" "~/.config/REAPER/Effects/"
ln -s "<local_path_to_this_repo>/PIDS.jsfx" "~/.config/REAPER/Effects/"
ln -s "<local_path_to_this_repo>/PIDS_with_PIDEG.jsfx" "~/.config/REAPER/Effects/"
```

## Usage

### Using the library in another JSFX script

#### Prerequisite

The script must define the following variables beforehand (ideally, provide them as user inputs):

**PID Gains**

- Kp => Proportional Component Gain Factor
- Ki => Integral Component Gain Factor
- Kd => Derivative Component Gain Factor

**Breakpoints**

- bp1y => Y-value for the first breakpoint
- bp2y => Y-Value for X-Value = 0.33 (second breakpoint)
- bp3y => Y-Value for X-Value = 0.67 (third breakpoint)
- bp1y => Y-value for the last breakpoint

**Oscillator-specific**

- linearize => 1 for linear artist, 0 for step artist
- octave_shift => Transposing one oscillator with respect to another (where 1 = 1 octave)

**Anti-aliasing**

- 2oversampling => Magnitude of oversampling; can be 1x, 2x, 4x or 8x

#### Steps

1. Importing the library

```java
import PIDS_lib.jsfx-inc
```

2. Using the library

```java
<object_name>.process<n>()
//where n = desired oversampling magnitude;
//example: pids.process8()
```

### Using the demo plugin in a REAPER project

1. Post installation, the plugin should appear in the FX List as `PIDS vx.x` & `PIDS vx.x (with PIDEG)`
2. Add the plugin to any FX chain. **Important: This plugin must the first one in the chain.**
3. Set/Modulate any of the provided inputs to control PIDS (& PIDEG) behaviour as desired.

![](docs/screen.png)

## Contributing

0. [Report Bugs or Request Features](https://gitlab.com/pidosc/pids-jsfx/-/issues)
1. Fork it (<https://gitlab.com/pidosc/pids-jsfx>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge/Pull Request

## License

Distributed under the Mozilla Public License Version 2.0 License. See `LICENSE` for more information.
